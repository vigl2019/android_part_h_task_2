package com.example.android_part_h_task_2.utils;

import java.util.List;

import com.example.android_part_h_task_2.model.Country;

public interface IHelper {

    void getCountries(List<Country> countries);
}