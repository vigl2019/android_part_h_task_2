package com.example.android_part_h_task_2;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.android_part_h_task_2.model.Country;

@Database(entities={Country.class /*, AnotherEntityType.class, AThirdEntityType.class */}, version = 1) // версия БД это целое число
public abstract class CountriesDataBase extends RoomDatabase {

    public abstract CountryDao getCountryDao();
}