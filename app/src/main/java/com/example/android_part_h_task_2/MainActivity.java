package com.example.android_part_h_task_2;

/*

1. Используя API сервиса https://restcountries.eu/ создать приложение с одним экраном.

Приложение должно содержать список стран, поле для ввода данных и кнопку фильтрации.

В текстовое поле пользователь вводит число, нажимает кнопку,
в списке появляются страны, у которых население соответствует указанному числу +/-15%.

При старте приложения должен посылаться запрос на получение данных с сервера и их сохранение в локальную DB.

Все фильтрации происходят с данными в локальной базе.

1.1 Добавить к предыдущему приложению кнопку для фильтрации по площади страны.

*/

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_part_h_task_2.model.Country;
import com.example.android_part_h_task_2.utils.Helper;
import com.example.android_part_h_task_2.utils.IHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    CountryAdapter countryAdapter;

    @BindView(R.id.am_input_number)
    EditText inputNumber;

    @BindView(R.id.am_get_countries_list_by_population)
    Button getCountriesListByPopulation;

    @BindView(R.id.am_get_countries_list_by_area)
    Button getCountriesListByArea;

    @BindView(R.id.am_country_list)
    RecyclerView recyclerView;

/*
    private IHelper iHelper = new IHelper() {
        @Override
        public void getCountries(List<Country> countries) {
            countryAdapter = new CountryAdapter(MainActivity.this, countries);
            recyclerView.setAdapter(countryAdapter);
        }
    };
*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        deleteDatabase("CountryDataBase");
        inputNumber.requestFocus();

        final Helper helper = new Helper();
        helper.getInfoFromServer(MainActivity.this); // Get info from server

        getCountriesListByPopulation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!inputNumber.getText().toString().isEmpty()) {

                    int population = Integer.parseInt(inputNumber.getText().toString().trim());

                    int minNumber = population - ((population * 15) / 100);
                    int maxNumber = population + ((population * 15) / 100);

                    IHelper iHelper = new IHelper() {
                        @Override
                        public void getCountries(List<Country> countries) {
                            countryAdapter = new CountryAdapter(MainActivity.this, countries);
                            recyclerView.setAdapter(countryAdapter);
                        }
                    };

                    helper.getCountriesByPopulationFromDataBase(minNumber, maxNumber, iHelper); // Get info from database
                }
                else {
                    Toast.makeText(MainActivity.this, "Input positive integer number!", Toast.LENGTH_LONG).show();
                    inputNumber.requestFocus();
                }
            }
        });

        getCountriesListByArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!inputNumber.getText().toString().isEmpty()) {

                    float area = Float.parseFloat(inputNumber.getText().toString().trim());

                    float minNumber = area - ((area * 15) / 100);
                    float maxNumber = area + ((area * 15) / 100);

                    IHelper iHelper = new IHelper() {
                        @Override
                        public void getCountries(List<Country> countries) {
                            countryAdapter = new CountryAdapter(MainActivity.this, countries);
                            recyclerView.setAdapter(countryAdapter);
                        }
                    };

                    helper.getCountriesByAreaFromDataBase(minNumber, maxNumber, iHelper); // Get info from database
                }
                else {
                    Toast.makeText(MainActivity.this, "Input positive float number!", Toast.LENGTH_LONG).show();
                    inputNumber.requestFocus();
                }
            }
        });
    }
}


